package denis.voronov.demousers

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.fragment.NavHostFragment
import dagger.hilt.android.AndroidEntryPoint
import denis.voronov.demousers.databinding.ActivityMainBinding
import denis.voronov.demousers.screens.usersList.UsersListFragmentDirections

@AndroidEntryPoint
class MainActivity : AppCompatActivity(), NavController.OnDestinationChangedListener {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val navHost = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val navController = navHost.navController

        navController.addOnDestinationChangedListener(this)

        binding.addUserFab.setOnClickListener {
            navController.navigate(UsersListFragmentDirections.fromUsersListToUserDerails(userId = -1))
        }
    }

    override fun onDestinationChanged(
        controller: NavController,
        destination: NavDestination,
        arguments: Bundle?
    ) {
        when (destination.id) {
            R.id.usersListFragment -> binding.addUserFab.visibility = View.VISIBLE
            else -> binding.addUserFab.visibility = View.GONE
        }
    }
}