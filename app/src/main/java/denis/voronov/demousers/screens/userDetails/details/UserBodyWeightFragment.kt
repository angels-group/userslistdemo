package denis.voronov.demousers.screens.userDetails.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import denis.voronov.demousers.R
import denis.voronov.demousers.databinding.FragmentUserBodyWeightBinding
import denis.voronov.demousers.repositories.models.User
import denis.voronov.demousers.screens.userDetails.UserDetailsInterface
import denis.voronov.demousers.screens.userDetails.UserDetailsViewModel

class UserBodyWeightFragment : Fragment(), UserDetailsInterface {

    private var _binding: FragmentUserBodyWeightBinding? = null
    private val binding get() = _binding

    private val userDetailsViewModel: UserDetailsViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentUserBodyWeightBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        userDetailsViewModel.user.observe(viewLifecycleOwner) { user ->
            user?.let { savedUser ->
                if (savedUser.bodyWeight != 0) {
                    binding?.userWeightET?.setText(savedUser.bodyWeight.toString())
                }

                if (savedUser.bodyWeightUnits.isNotEmpty()) {
                    if (savedUser.bodyWeightUnits == User.KG_UNIT) {
                        binding?.unitsGroup?.check(R.id.unitsKg)
                    } else {
                        binding?.unitsGroup?.check(R.id.unitsLb)
                    }
                }
            }
        }
    }

    override fun isFieldsFilled(): Boolean = binding?.userWeightET?.text.toString().isNotEmpty()

    override fun updateData() {
        val units = if (binding?.unitsGroup?.checkedButtonId == R.id.unitsKg) {
            User.KG_UNIT
        } else {
            User.LB_UNIT
        }
        userDetailsViewModel.setUserWeight(binding?.userWeightET?.text.toString(), units)
    }
}