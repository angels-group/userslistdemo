package denis.voronov.demousers.screens.usersList

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import denis.voronov.demousers.databinding.ItemUserBinding
import denis.voronov.demousers.repositories.models.User

class UsersAdapter(
    private val onEditUserClick: (user: User) -> Unit
): ListAdapter<User, UserViewHolder>(UsersDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = UserViewHolder(
        itemBinding = ItemUserBinding.inflate(LayoutInflater.from(parent.context), parent, false),
        onEditUserClick = onEditUserClick
    )

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

class UsersDiffCallback: DiffUtil.ItemCallback<User>(){
    override fun areItemsTheSame(oldItem: User, newItem: User): Boolean = oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
        return oldItem.bodyWeight == newItem.bodyWeight
                && oldItem.dateOfBirth == newItem.dateOfBirth
                && oldItem.bodyWeightUnits == newItem.bodyWeightUnits
                && oldItem.photoUri == newItem.photoUri
    }
}