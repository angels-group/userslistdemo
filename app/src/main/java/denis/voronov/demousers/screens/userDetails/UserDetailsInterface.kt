package denis.voronov.demousers.screens.userDetails

interface UserDetailsInterface {
    fun isFieldsFilled(): Boolean
    fun updateData()
}