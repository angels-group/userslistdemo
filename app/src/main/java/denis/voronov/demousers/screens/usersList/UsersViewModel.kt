package denis.voronov.demousers.screens.usersList

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import denis.voronov.demousers.repositories.UsersRepo
import denis.voronov.demousers.repositories.models.User
import javax.inject.Inject

@HiltViewModel
class UsersViewModel @Inject constructor(usersRepo: UsersRepo) : ViewModel() {
    val users: LiveData<List<User>> = usersRepo.getAllUsers().asLiveData()
}