package denis.voronov.demousers.screens.usersList

import android.annotation.SuppressLint
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import denis.voronov.demousers.R
import denis.voronov.demousers.databinding.ItemUserBinding
import denis.voronov.demousers.repositories.models.User
import java.text.SimpleDateFormat
import java.util.*

class UserViewHolder(
    private val itemBinding: ItemUserBinding,
    onEditUserClick: (user: User) -> Unit
): RecyclerView.ViewHolder(itemBinding.root) {

    private var user: User? = null

    init {
        itemBinding.editUserButton.setOnClickListener {
            user?.let(onEditUserClick)
        }
    }

    @SuppressLint("SetTextI18n")
    fun bind(user: User) {
        this.user = user

        Glide.with(itemView.context)
            .load(user.photoUri)
            .into(itemBinding.userPhoto)

        itemBinding.userWeightData.text = "${user.bodyWeight}${user.bodyWeightUnits}"

        val dateFormat = SimpleDateFormat(
            itemView.context.getString(R.string.date_pattern), Locale.getDefault()
        )

        itemBinding.userDobData.text = dateFormat.format(user.dateOfBirth)
    }
}