package denis.voronov.demousers.screens.userDetails

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.navArgs
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint
import denis.voronov.demousers.R
import denis.voronov.demousers.databinding.FragmentUserDetailsBinding
import denis.voronov.demousers.screens.userDetails.details.UserStepsFactory

@AndroidEntryPoint
class UserDetailsFragment : Fragment() {

    private var _binding: FragmentUserDetailsBinding? = null
    private val binding get() = _binding

    private val args: UserDetailsFragmentArgs by navArgs()

    private val userDetailsViewModel: UserDetailsViewModel by activityViewModels()

    private var stepsAdapter: DetailsPageAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentUserDetailsBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onDetach() {
        super.onDetach()
        userDetailsViewModel.clearUser()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        stepsAdapter = DetailsPageAdapter(this)
        stepsAdapter?.setSteps(UserStepsFactory.prepareSteps())

        binding?.viewPager?.apply {
            adapter = stepsAdapter
            offscreenPageLimit = stepsAdapter?.itemCount ?: 1
            isUserInputEnabled = false
            registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    binding?.viewPager?.adapter?.itemCount?.let { totalCount ->
                        if (position == totalCount - 1) {
                            binding?.nextButton?.text = getString(R.string.button_done)
                        } else {
                            binding?.nextButton?.text = getString(R.string.button_next)
                        }
                    }
                }
            })
        }

        if (args.userId == -1) {
            userDetailsViewModel.createEmptyUser()
        } else {
            userDetailsViewModel.getUserById(args.userId)
        }

        binding?.nextButton?.setOnClickListener {
            binding?.viewPager?.currentItem?.let { currentPage ->
                stepsAdapter?.itemCount?.let { totalCount ->
                    if (stepsAdapter?.isNextStepAvailable(currentPage) == true) {
                        if (currentPage == totalCount - 1) {
                            userDetailsViewModel.saveUser()
                            (activity?.supportFragmentManager?.findFragmentById(R.id.nav_host_fragment) as NavHostFragment)
                                .navController.navigate(
                                    UserDetailsFragmentDirections.fromUserDetailsToUsersList()
                                )
                        } else {
                            stepsAdapter?.saveData(currentPage)
                            binding?.viewPager?.setCurrentItem(currentPage + 1, true)
                        }
                    } else {
                        binding?.root?.let { view ->
                            // ToDo highlight required fields in UI
                            Snackbar.make(view, getString(R.string.snack_fill_all_fields), Snackbar.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        }

        binding?.previousButton?.setOnClickListener {
            binding?.viewPager?.currentItem?.let { currentPage ->
                if (currentPage == 0) {
                    (activity?.supportFragmentManager?.findFragmentById(R.id.nav_host_fragment) as NavHostFragment)
                        .navController.navigateUp()
                } else {
                    binding?.viewPager?.setCurrentItem(currentPage - 1, true)
                }
            }
        }

        binding?.pageIndicator?.let { pageIndicator ->
            binding?.viewPager?.let { viewPager ->
                TabLayoutMediator(pageIndicator, viewPager) { tab: TabLayout.Tab, _: Int ->
                    tab.view.isClickable = false
                }.attach()
            }
        }
    }
}