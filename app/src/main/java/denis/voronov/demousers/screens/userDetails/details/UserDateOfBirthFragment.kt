package denis.voronov.demousers.screens.userDetails.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.google.android.material.datepicker.MaterialDatePicker
import denis.voronov.demousers.R
import denis.voronov.demousers.databinding.FragmentUserDateOfBirthBinding
import denis.voronov.demousers.screens.userDetails.UserDetailsInterface
import denis.voronov.demousers.screens.userDetails.UserDetailsViewModel
import java.text.SimpleDateFormat
import java.util.*

class UserDateOfBirthFragment : Fragment(), UserDetailsInterface {

    private var _binding: FragmentUserDateOfBirthBinding? = null
    private val binding get() = _binding

    private val userDetailsViewModel: UserDetailsViewModel by activityViewModels()

    private var datePicker: MaterialDatePicker<Long?>? = null

    private var dateFormat: SimpleDateFormat? = null

    private var newDate: Date? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentUserDateOfBirthBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dateFormat = SimpleDateFormat(getString(R.string.date_pattern), Locale.getDefault())

        userDetailsViewModel.user.observe(viewLifecycleOwner) { user ->
            if (user?.dateOfBirth != Date(0)) {
                user?.dateOfBirth?.let { savedDate ->
                    newDate = savedDate
                    dateFormat?.let {
                        binding?.userDobET?.setText(it.format(savedDate))
                    }
                }
            }
        }

        // ToDo add constraints
        // FixMe setSelection when edit existing user
        datePicker = MaterialDatePicker.Builder.datePicker()
            .setTitleText(getString(R.string.date_picker_title))
            .setSelection(MaterialDatePicker.todayInUtcMilliseconds())
            .build()

        binding?.userDobTL?.setEndIconOnClickListener {
            datePicker?.show(childFragmentManager, "datePicker")
        }

        datePicker?.addOnPositiveButtonClickListener {
            it?.let { selectedDate ->
                newDate = Date(selectedDate)
                dateFormat?.let {
                    binding?.userDobET?.setText(it.format(selectedDate))
                }
            }
        }
    }

    override fun isFieldsFilled(): Boolean  = binding?.userDobET?.text.toString().isNotEmpty()

    override fun updateData() {
        newDate?.let { userDetailsViewModel.setUserDateOfBirth(it) }
    }

}