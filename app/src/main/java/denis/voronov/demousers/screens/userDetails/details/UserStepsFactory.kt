package denis.voronov.demousers.screens.userDetails.details

import androidx.fragment.app.Fragment

object UserStepsFactory {

    fun prepareSteps() = arrayOf<Fragment>(
        UserBodyWeightFragment(),
        UserDateOfBirthFragment(),
        UserPhotoFragment()
    )

}