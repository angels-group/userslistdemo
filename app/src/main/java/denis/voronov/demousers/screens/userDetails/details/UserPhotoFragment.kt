package denis.voronov.demousers.screens.userDetails.details

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.bumptech.glide.Glide
import denis.voronov.demousers.BuildConfig
import denis.voronov.demousers.databinding.FragmentUserPhotoBinding
import denis.voronov.demousers.screens.userDetails.UserDetailsInterface
import denis.voronov.demousers.screens.userDetails.UserDetailsViewModel
import java.io.File

class UserPhotoFragment : Fragment(), UserDetailsInterface {

    private var _binding: FragmentUserPhotoBinding? = null
    private val binding get() = _binding

    private val userDetailsViewModel: UserDetailsViewModel by activityViewModels()

    private var tempUri: Uri? = null

    private val selectImageFromGalleryResult = registerForActivityResult(ActivityResultContracts.GetContent()) { imageUri ->
        imageUri?.let {
            tempUri = it
            userDetailsViewModel.setUserPhoto(tempUri)
            setImage()
        }
    }

    private val selectImageFromCameraResult = registerForActivityResult(ActivityResultContracts.TakePicture()) { isSuccess ->
        if (isSuccess) {
            userDetailsViewModel.setUserPhoto(tempUri)
            setImage()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentUserPhotoBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.galleryButton?.setOnClickListener {
            pickFromGallery()
        }

        binding?.cameraButton?.setOnClickListener {
            pickFromCamera()
        }

        userDetailsViewModel.user.observe(viewLifecycleOwner) { user ->
            user?.let {
                if (user.photoUri.isNotEmpty()) {
                    tempUri = Uri.parse(user.photoUri)
                    setImage()
                }
            }
        }
    }

    private fun pickFromGallery() = selectImageFromGalleryResult.launch("image/*")

    private fun pickFromCamera() {
        tempUri = createTempFile()
        selectImageFromCameraResult.launch(tempUri)
    }

    private fun createTempFile(): Uri {
        val tmp = File.createTempFile("tmp_user_avatar_", ".png", activity?.cacheDir).apply {
            createNewFile()
            deleteOnExit()
        }

        return FileProvider.getUriForFile(requireContext(), "${BuildConfig.APPLICATION_ID}.provider", tmp)
    }

    private fun setImage() {
        binding?.userPhoto?.let { imageView ->
            Glide.with(imageView.context)
                .load(tempUri)
                .into(imageView)
        }
    }

    override fun isFieldsFilled(): Boolean = userDetailsViewModel.user.value?.photoUri?.isNotBlank() ?: false

    override fun updateData() {}
}