package denis.voronov.demousers.screens.userDetails

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import denis.voronov.demousers.repositories.UsersRepo
import denis.voronov.demousers.repositories.models.User
import java.util.*
import javax.inject.Inject

@HiltViewModel
class UserDetailsViewModel @Inject constructor(private val usersRepo: UsersRepo): ViewModel() {

    private val _user = MutableLiveData<User?>()
    val user: LiveData<User?> = _user

    fun getUserById(id: Int) {
        viewModelScope.launch (Dispatchers.IO) {
            _user.postValue(usersRepo.getUserById(id))
        }
    }

    fun createEmptyUser() {
        _user.postValue(
            User(
                id = -1,
                bodyWeight = 0,
                bodyWeightUnits = "",
                dateOfBirth = Date(0),
                photoUri = ""
            )
        )
    }

    fun setUserWeight(weight: String, units: String) {
        _user.value?.let { user ->
            _user.postValue(
                user.copy(
                    bodyWeight = weight.toInt(),
                    bodyWeightUnits = units
                )
            )
        }
    }

    fun setUserDateOfBirth(date: Date){
        _user.value?.let { user ->
            _user.postValue(
                user.copy(
                    dateOfBirth = date
                )
            )
        }
    }

    fun setUserPhoto(uri: Uri?){
        if (uri == null) return
        _user.value?.let { user ->
            _user.postValue(
                user.copy(
                    photoUri = uri.toString()
                )
            )
        }
    }

    fun clearUser() {
        _user.postValue(null)
    }

    fun saveUser() {
        viewModelScope.launch (Dispatchers.IO) {
            _user.value?.let {
                usersRepo.saveUser(it)
            }
        }
    }
}