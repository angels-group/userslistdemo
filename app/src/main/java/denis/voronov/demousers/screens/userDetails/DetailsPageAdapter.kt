package denis.voronov.demousers.screens.userDetails

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

class DetailsPageAdapter(fragment: Fragment): FragmentStateAdapter(fragment) {

    private val fragments: MutableList<Fragment> = ArrayList()

    override fun getItemCount(): Int = fragments.size

    override fun createFragment(position: Int): Fragment = fragments[position]

    fun setSteps(steps: Array<Fragment>) {
        fragments.addAll(steps)
    }

    fun isNextStepAvailable(position: Int): Boolean {
        return (fragments[position] as UserDetailsInterface).isFieldsFilled()
    }

    fun saveData(position: Int) {
        (fragments[position] as UserDetailsInterface).updateData()
    }
}