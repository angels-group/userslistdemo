package denis.voronov.demousers.screens.usersList

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import denis.voronov.demousers.R
import denis.voronov.demousers.databinding.FragmentUsersListBinding
import denis.voronov.demousers.repositories.models.User

@AndroidEntryPoint
class UsersListFragment : Fragment() {

    private var _binding: FragmentUsersListBinding? = null
    private val binding get() = _binding

    private val usersViewModel: UsersViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentUsersListBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val usersAdapter = UsersAdapter(::editUser)

        binding?.usersListRecyclerView?.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = usersAdapter
            addItemDecoration(DividerItemDecoration(requireContext(), LinearLayoutManager.VERTICAL))
        }

        usersViewModel.users.observe(viewLifecycleOwner) {
            if (it.isEmpty()) {
                binding?.noUsersView?.visibility = View.VISIBLE
            } else {
                usersAdapter.submitList(it)
                binding?.noUsersView?.visibility = View.GONE
            }
        }
    }

    private fun editUser(user: User) {
        (activity?.supportFragmentManager?.findFragmentById(R.id.nav_host_fragment) as NavHostFragment)
            .navController.navigate(
                UsersListFragmentDirections.fromUsersListToUserDerails(userId = user.id)
            )
    }
}