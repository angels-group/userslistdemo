package denis.voronov.demousers.repositories.models

import denis.voronov.demousers.database.entity.UserEntity
import java.util.*

data class User (
    val id: Int,
    val bodyWeight: Int,
    val bodyWeightUnits: String,
    val dateOfBirth: Date,
    val photoUri: String
) {
    companion object {
        const val KG_UNIT = "kg"
        const val LB_UNIT = "lb"
    }
}

fun User.toEntity() = UserEntity (
    id = id,
    bodyWeight = bodyWeight,
    bodyWeightUnits = bodyWeightUnits,
    dateOfBirth = dateOfBirth,
    photoUri = photoUri
)