package denis.voronov.demousers.repositories

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import denis.voronov.demousers.database.Database
import denis.voronov.demousers.database.entity.UserEntity
import denis.voronov.demousers.database.entity.toUser
import denis.voronov.demousers.repositories.models.User
import denis.voronov.demousers.repositories.models.toEntity
import javax.inject.Inject

class UsersRepo @Inject constructor(database: Database) {

    private val usersDao = database.users()

    fun getAllUsers(): Flow<List<User>> {
        return usersDao.getAllUsers()
            .map { users ->
                users.map {
                    it.toUser()
                }
            }
    }

    suspend fun getUserById(id: Int) = usersDao.getUserById(id).toUser()

    suspend fun saveUser(user: User) {
        if (user.id == -1) {
            usersDao.addUser(
                UserEntity(
                    bodyWeight = user.bodyWeight,
                    bodyWeightUnits = user.bodyWeightUnits,
                    dateOfBirth = user.dateOfBirth,
                    photoUri = user.photoUri
                )
            )
        } else {
            usersDao.updateUser(user.toEntity())
        }
    }
}