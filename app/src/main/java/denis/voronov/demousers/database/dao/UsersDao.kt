package denis.voronov.demousers.database.dao

import androidx.room.*
import kotlinx.coroutines.flow.Flow
import denis.voronov.demousers.database.entity.UserEntity
import denis.voronov.demousers.database.entity.UserEntity.Companion.COLUMN_ID
import denis.voronov.demousers.database.entity.UserEntity.Companion.TABLE_NAME

@Dao
interface UsersDao {

    @Query("SELECT * FROM $TABLE_NAME")
    fun getAllUsers(): Flow<List<UserEntity>>

    @Query("SELECT * FROM $TABLE_NAME WHERE $COLUMN_ID = :id")
    suspend fun getUserById(id: Int): UserEntity

    @Update
    suspend fun updateUser(userEntity: UserEntity)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addUser(userEntity: UserEntity)
}