package denis.voronov.demousers.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import denis.voronov.demousers.database.converters.DateConverter
import denis.voronov.demousers.database.entity.UserEntity.Companion.TABLE_NAME
import denis.voronov.demousers.repositories.models.User
import java.util.*


@Entity(tableName = TABLE_NAME)
@TypeConverters(DateConverter::class)
class UserEntity(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = COLUMN_ID)
    val id: Int? = null,

    @ColumnInfo(name = COLUMN_BODY_WEIGHT)
    val bodyWeight: Int,

    @ColumnInfo(name = COLUMN_BODY_WEIGHT_UNITS)
    val bodyWeightUnits: String,

    @ColumnInfo(name = COLUMN_DATE_OF_BIRTH)
    val dateOfBirth: Date,

    @ColumnInfo(name = COLUMN_PHOTO_URI)
    val photoUri: String

) {

    companion object {
        const val TABLE_NAME = "users"

        const val COLUMN_ID = "_id"
        const val COLUMN_BODY_WEIGHT = "body_weight"
        const val COLUMN_BODY_WEIGHT_UNITS = "body_weight_units"
        const val COLUMN_DATE_OF_BIRTH = "date_of_birth"
        const val COLUMN_PHOTO_URI = "photo_uri"
    }
}

fun UserEntity.toUser(): User = User(
    id = id ?: -1,
    bodyWeight = bodyWeight,
    bodyWeightUnits = bodyWeightUnits,
    dateOfBirth = dateOfBirth,
    photoUri = photoUri
)