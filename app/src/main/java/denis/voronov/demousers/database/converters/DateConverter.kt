package denis.voronov.demousers.database.converters

import androidx.room.TypeConverter
import java.util.*

object DateConverter {

    @JvmStatic
    @TypeConverter
    fun serialize(date: Date): Long {
        return date.time
    }

    @JvmStatic
    @TypeConverter
    fun deserialize(serialized: Long): Date {
        return Date(serialized)
    }
}