package denis.voronov.demousers.database

import androidx.room.Database
import androidx.room.RoomDatabase
import denis.voronov.demousers.database.dao.UsersDao
import denis.voronov.demousers.database.entity.UserEntity

@Database(
    entities = [
        UserEntity::class
    ],
    version = 1
)
abstract class Database : RoomDatabase() {
    abstract fun users() : UsersDao
}
